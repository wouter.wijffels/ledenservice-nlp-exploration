import requests
import pandas as pd
import base64
from requests.exceptions import RequestException
from datetime import datetime
import time

def gather_conversations(environment, client_id, client_secret, start_time, end_time, max_retries=3, retry_delay=60):
    """
    Fetches conversation IDs from Genesys API for the given time range (start_time to end_time).
    Returns both the conversation DataFrame and the authorization headers.
    """
    # Prepare authorization headers for token request
    authorization = base64.b64encode(f"{client_id}:{client_secret}".encode("ISO-8859-1")).decode("ascii")
    token_headers = {
        "Authorization": f"Basic {authorization}",
        "Content-Type": "application/x-www-form-urlencoded"
    }
    token_body = {"grant_type": "client_credentials"}

    # Request a token from the Genesys API
    response = requests.post(f"https://login.{environment}/oauth/token", data=token_body, headers=token_headers)
    if response.status_code != 200:
        print(f"Failed to authenticate: {response.status_code} - {response.reason}")
        return pd.DataFrame(), None  # Return empty DataFrame and None on failure

    response_json = response.json()
    # Create the authorization header for subsequent requests
    detail_headers = {
        "Authorization": f"{response_json['token_type']} {response_json['access_token']}",
        "Content-Type": "application/json"
    }

    # Initialize an empty DataFrame to store results
    df_conversations = pd.DataFrame(columns=["date", "communication_id", "conversation_id", "conversation"])

    # Prepare the API request body with the specified time interval
    detail_body = {
        "order": "asc",
        "orderBy": "conversationStart",
        "paging": {
            "pageSize": 99,
            "pageNumber": 1
        },
        "interval": f"{start_time.isoformat()}/{end_time.isoformat()}",
        "segmentFilters": [
            {
                "type": "and",
                "predicates": [
                    {"type": "dimension", "dimension": "mediaType", "operator": "matches", "value": "voice"},
                    {"type": "dimension", "dimension": "purpose", "operator": "matches", "value": "customer"},
                    {"type": "dimension", "dimension": "recording", "operator": "exists", "value": None}
                ]
            }
        ]
    }

    retries = 0
    while retries < max_retries:
        try:
            # Send API request for the time instance
            response = requests.post(f"https://api.{environment}/api/v2/analytics/conversations/details/query",
                                     json=detail_body, headers=detail_headers)
            if response.status_code == 200:
                # Parse the JSON response
                json_response = response.json()

                # Extract relevant data from the response
                for conversation in json_response.get('conversations', []):
                    for participant in conversation.get('participants', []):
                        if participant.get('purpose') == 'customer':
                            for information in participant.get('sessions', []):
                                if information.get('mediaType') == 'voice':
                                    new_row = {
                                        "date": conversation.get('conversationStart'),
                                        "communication_id": information.get('sessionId'),
                                        "conversation_id": conversation.get('conversationId'),
                                        "conversation": conversation
                                    }
                                    # Append the new data to the DataFrame
                                    df_conversations = pd.concat([df_conversations, pd.DataFrame([new_row])], ignore_index=True)

                break  # Exit retry loop on success

            else:
                retries += 1
                print(f"Request failed with status code: {response.status_code}. Retrying {retries}/{max_retries}...")
                time.sleep(retry_delay)

        except RequestException as e:
            retries += 1
            print(f"Request exception: {e}. Retrying {retries}/{max_retries}...")
            time.sleep(retry_delay)

    return df_conversations, detail_headers  # Return DataFrame and authorization headers
