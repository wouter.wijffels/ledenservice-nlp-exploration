import os
import sys
import pandas as pd
from datetime import datetime, timedelta
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()

# Importing functions from other Python files
from get_conversation_ids import gather_conversations
from fetch_transcripts import fetch_conversation_transcripts
from snowflake_utils import write_df_to_snowflake, truncate_table, get_max_timestamp

def main():
    # Load environment variables
    environment = os.environ.get("PURECLOUD_ENVIRONMENT")
    client_id = os.environ.get("PURECLOUD_CLIENT_ID")
    client_secret = os.environ.get("PURECLOUD_CLIENT_SECRET")
    snowflake_table = os.environ.get("SNOWFLAKE_TABLE")

    if not (environment and client_id and client_secret and snowflake_table):
        print("Error: Missing required environment variables.")
        sys.exit(1)

    # Fetch the maximum timestamp from Snowflake and set the start time
    print("Fetching maximum timestamp from Snowflake...")
    max_timestamp = get_max_timestamp(snowflake_table)

    if max_timestamp is None:
        print("No data in Snowflake table. Starting from the default timestamp.")
        start_time = datetime(2024, 3, 1, 9, 0, 0)  # Default start time
    else:
        start_time = max_timestamp + timedelta(minutes=25)
    print(f"Start time for processing: {start_time}")

    # Define a 5-minute window
    end_time = start_time + timedelta(minutes=5)

    print(f"Processing data for the window {start_time} to {end_time}.")

    # Step 1: Gather conversation IDs within the 5-minute window
    print(f"Gathering conversation IDs for {start_time} to {end_time}...")
    df_conversations, convo_headers = gather_conversations(
        environment,
        client_id,
        client_secret,
        start_time,
        end_time
    )

    if df_conversations.empty:
        print(f"No conversations found for the interval {start_time} to {end_time}. Exiting.")
        return

    print(f"Gathered {len(df_conversations)} conversations for the window.")

    df_conversations['result'] = None
    try:
        # Fetch and mask the conversation transcript
        df_conversations = fetch_conversation_transcripts(
            df_conversations,  # Pass the row as a DataFrame
            convo_headers=convo_headers
        )


    except Exception as e:
        print(f"Error processing conversation ID {df_conversations['conversation_id']}: {e}")
        df_conversations.loc['result'] = "Error processing conversation"

        # Step 3: Save the DataFrame to a CSV file
    output_file = 'filter_test.csv'
    print(f"Saving results to {output_file}...")
    df_conversations.to_csv(output_file, sep=',', index=False)
    print(f"Results saved successfully to {output_file}.")

if __name__ == "__main__":
    main()
