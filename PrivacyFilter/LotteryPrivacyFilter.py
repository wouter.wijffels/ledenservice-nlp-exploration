import os
import re
import csv
import pandas as pd


class LotteryPrivacyFilter:
    """
    Class to filter PII sensitive information from a text. Typical information are
    names, postcodes, cities, bank details.
    """

    def __init__(self, clean_accents=True, nlp_filter=True,
                 wordlist_filter=True, regular_expressions=True):
        # Store parameters
        self.clean_accents = clean_accents
        self.nlp_filter = nlp_filter
        self.wordlist_filter = wordlist_filter
        self.regular_expressions = regular_expressions

        # Dynamically locate the datasets directory relative to the project root
        self.project_root = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
        self.datadir = os.path.join(self.project_root, "PrivacyFilter", "datasets")

        # Initialize fields for filtering
        self.fields = {
            os.path.join(self.datadir, 'numbers.csv'): {"replacement": "<GETAL>", "punctuation": None},
            os.path.join(self.datadir, 'bank_information.csv'): {"replacement": "<BANK>", "punctuation": None},
            os.path.join(self.datadir, 'firstnames.csv'): {"replacement": "<NAAM>", "punctuation": None},
            os.path.join(self.datadir, 'lastnames.csv'): {"replacement": "<NAAM>", "punctuation": None},
            os.path.join(self.datadir, 'places.csv'): {"replacement": "<PLAATS>", "punctuation": None},
            os.path.join(self.datadir, 'streets_Nederland.csv'): {"replacement": "<ADRES>", "punctuation": None},
            os.path.join(self.datadir, 'diseases.csv'): {"replacement": "<AANDOENING>", "punctuation": None},
            os.path.join(self.datadir, 'medicines.csv'): {"replacement": "<MEDICIJN>", "punctuation": None},
            os.path.join(self.datadir, 'nationalities.csv'): {"replacement": "<NATIONALITEIT>", "punctuation": None},
            os.path.join(self.datadir, 'countries.csv'): {"replacement": "<LAND>", "punctuation": None},
            os.path.join(self.datadir, 'months.csv'): {"replacement": "<MONTH>", "punctuation": None},
            os.path.join(self.datadir, 'email.csv'): {"replacement": "<EMAIL>", "punctuation": None},
        }

        # Load field data
        self.field_replacements = self.load_field_replacements()

    def load_field_replacements(self):
        """
        Load replacements from the CSV files specified in self.fields.
        """
        field_replacements = {}
        for filepath, info in self.fields.items():
            if os.path.exists(filepath):
                with open(filepath, 'r', encoding='utf-8') as f:
                    reader = csv.reader(f)
                    # Storing each entry for replacement
                    field_replacements[info['replacement']] = [row[0].strip() for row in reader]
            else:
                print(f"Warning: {filepath} does not exist.")
        return field_replacements

    @staticmethod
    def remove_bank_details(text):
        iban_pattern = r"\bNL(?:\s?\d|[A-Za-z]+){2}\s?(ABNA|INGB|RABO|SNSB|ASNB|KNAB|BUNQ|TRIO|MOYO|HAND)(?:\s?\d|[A-Za-z]+){2,10}\b"
        spaced_bank_codes = r"\b(?:A\s?B\s?N\s?A|I\s?N\s?G\s?B|R\s?A\s?B\s?O|S\s?N\s?S\s?B|A\s?S\s?N\s?B|K\s?N\s?A\s?B|B\s?U\s?N\s?Q|T\s?R\s?I\s?O|M\s?O\s?Y\s?O|H\s?A\s?N\s?D)\b"

        text = re.sub(iban_pattern, "<BANKREKENING>", text, flags=re.IGNORECASE)
        text = re.sub(spaced_bank_codes, "<BANKNAAM>", text, flags=re.IGNORECASE)

        return text

    @staticmethod
    def remove_number_words(text):
        pattern = r"\b\w*(?:ntwintig|n(?:(?:dertig)|(?:veertig)|(?:ftig)|(?:zestig)|(?:ventig)|(?:nentig)))\b"
        text = re.sub(pattern, "<GETAL>", text, flags=re.IGNORECASE)

        return text

    def filter(self, text):
        """
        Filter the provided text by replacing fields and removing bank details.
        """
        # Replace fields from loaded data
        for replacement, entries in self.field_replacements.items():
            # Create a pattern for the names and places with optional punctuations
            pattern = r'\b(?:' + '|'.join(map(re.escape, entries)) + r')\b'
            text = re.sub(pattern, replacement, text, flags=re.IGNORECASE)

        # Remove bank details and manual numeric words
        text = self.remove_bank_details(text)
        text = self.remove_number_words(text)

        text = re.sub(r'\b(hallo|goeiedag|goedemorgen|goedemiddag|goedenavond|spreekt|vriendenloterij|postcode loterij) met\b\s+(\w+)(?:\s+\w+)?',
                      r'\1 met <NAAM>',
                      text,
                      flags=re.IGNORECASE)

        return text
