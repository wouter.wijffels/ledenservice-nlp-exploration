import os
import pandas as pd
from snowflake.connector import connect


def write_df_to_snowflake(df, table_name):
    """
    Writes a DataFrame to a specified table in Snowflake row by row using external browser authentication.

    Parameters:
        df (pd.DataFrame): DataFrame to be written to Snowflake.
        table_name (str): Target table name in Snowflake.
    """
    # Establish connection to Snowflake
    conn = connect(
        user=os.environ.get("SNOWFLAKE_USERNAME"),
        account=os.environ.get("SNOWFLAKE_ACCOUNT"),
        warehouse=os.environ.get("SNOWFLAKE_WAREHOUSE"),
        database=os.environ.get("SNOWFLAKE_DATABASE"),
        schema=os.environ.get("SNOWFLAKE_SCHEMA"),
        role=os.environ.get("SNOWFLAKE_ROLE"),  # Optional: use if roles are enabled
        authenticator="externalbrowser"
    )
    cursor = conn.cursor()

    try:
        # Iterate over each row in the DataFrame and insert into Snowflake
        for _, row in df.iterrows():
            row_dict = row.to_dict()

            # Generate column names and values for the INSERT query
            columns = ', '.join(row_dict.keys())
            placeholders = ', '.join(['%s'] * len(row_dict))
            values = list(row_dict.values())

            insert_query = f"INSERT INTO {table_name} ({columns}) VALUES ({placeholders})"

            cursor.execute(insert_query, values)
            print(f"Inserted row into {table_name}: {row_dict}")

        # Commit the transaction after inserting all rows
        conn.commit()
        print(f"All rows successfully inserted into {table_name}.")

    except Exception as e:
        print(f"Error occurred while writing to Snowflake: {e}")
        conn.rollback()

    finally:
        cursor.close()
        conn.close()


def execute_snowflake_query(query, params=None):
    """
    Executes a query against the Snowflake database and returns the result as a pandas DataFrame using external browser authentication.

    Parameters:
        query (str): The SQL query to execute.
        params (tuple, optional): Parameters to substitute in the SQL query. Default is None.

    Returns:
        pd.DataFrame: Result of the query as a DataFrame.
    """
    conn = connect(
        user=os.environ.get("SNOWFLAKE_USERNAME"),
        account=os.environ.get("SNOWFLAKE_ACCOUNT"),
        warehouse=os.environ.get("SNOWFLAKE_WAREHOUSE"),
        database=os.environ.get("SNOWFLAKE_DATABASE"),
        schema=os.environ.get("SNOWFLAKE_SCHEMA"),
        role=os.environ.get("SNOWFLAKE_ROLE"),
        authenticator="externalbrowser"
    )

    try:
        cursor = conn.cursor()
        cursor.execute(query, params)
        result = cursor.fetchall()
        columns = [col[0] for col in cursor.description]
        df = pd.DataFrame(result, columns=columns)
        return df

    except Exception as e:
        print(f"Error occurred while executing query: {e}")
        return pd.DataFrame()

    finally:
        cursor.close()
        conn.close()


def get_max_timestamp(table_name):
    """
    Fetches the maximum date from a specified table in Snowflake.

    Parameters:
        table_name (str): Name of the Snowflake table.

    Returns:
        datetime or None: The maximum date from the table, or None if the table is empty or inaccessible.
    """
    query = f"SELECT MAX(DATE) AS MAX_DATE FROM {table_name}"

    # Execute the query and fetch the result
    result = execute_snowflake_query(query)

    # Debugging: Print the result to understand the DataFrame's structure
    print(f"Query result: {result}")

    # Check if the DataFrame is empty or if 'max_date' is missing
    if result.empty:
        print("The query returned an empty result.")
        return None

    if 'MAX_DATE' not in result.columns:
        print("The 'MAX_DATE' column is not present in the result.")
        return None

    # Return the value of 'max_date' if it exists
    return result.iloc[0]['MAX_DATE']


def truncate_table(table_name):
    """
    Truncates the specified table in Snowflake using external browser authentication.

    Parameters:
        table_name (str): The name of the table to be truncated.
    """
    conn = connect(
        user=os.environ.get("SNOWFLAKE_USERNAME"),
        account=os.environ.get("SNOWFLAKE_ACCOUNT"),
        warehouse=os.environ.get("SNOWFLAKE_WAREHOUSE"),
        database=os.environ.get("SNOWFLAKE_DATABASE"),
        schema=os.environ.get("SNOWFLAKE_SCHEMA"),
        role=os.environ.get("SNOWFLAKE_ROLE"),
        authenticator="externalbrowser"
    )
    cursor = conn.cursor()

    try:
        truncate_command = f"TRUNCATE TABLE {table_name}"
        cursor.execute(truncate_command)
        print(f"Table {table_name} successfully truncated.")

    except Exception as e:
        print(f"Error occurred while truncating table {table_name}: {e}")

    finally:
        cursor.close()
        conn.close()
