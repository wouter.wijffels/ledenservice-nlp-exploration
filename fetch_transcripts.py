import requests
import time
from PrivacyFilter.LotteryPrivacyFilter import LotteryPrivacyFilter

def fetch_conversation_transcripts(df_conversations, convo_headers):
    """
    Fetches conversation transcripts for each row in the DataFrame and appends the combined phrases
    to a 'conversation' column. Applies a privacy filter and stores the filtered text in 'result'.

    Parameters:
        df_conversations (pd.DataFrame): DataFrame containing 'communication_id' and 'conversation_id' columns.
        convo_headers (dict): Headers to include in API requests, typically with authorization details.

    Returns:
        pd.DataFrame: Updated DataFrame with 'conversation' and 'result' columns.
    """
    start_time = time.time()  # Start timing the process

    for index, row in df_conversations.iterrows():
        try:
            # Access the elements of each row
            communication_id = row['communication_id']
            conversation_id = row['conversation_id']

            # API call to fetch the transcript URL
            response = requests.get(
                f'https://api.mypurecloud.ie/api/v2/speechandtextanalytics/conversations/{conversation_id}/communications/{communication_id}/transcripturl',
                headers=convo_headers
            )
            response.raise_for_status()

            url_json = response.json()
            url = url_json.get('url')
            if not url:
                continue  # Skip if 'url' is not present

            # Fetch the transcript data
            text_response = requests.get(url)
            text_response.raise_for_status()
            text_json = text_response.json()

            combined_phrases = ""  # Initialize to store conversation phrases

            # Combine the phrases from the transcript
            for transcript in text_json.get('transcripts', []):
                for phrase in transcript.get('phrases', []):
                    if phrase.get('participantPurpose') == 'external':
                        combined_phrases += f"[customer] {phrase.get('text', '')}."
                    elif phrase.get('participantPurpose') == 'internal':
                        combined_phrases += f"[agent] {phrase.get('text', '')}."

            # Update the DataFrame with the unfiltered conversation
            df_conversations.at[index, 'conversation'] = combined_phrases

        except requests.exceptions.RequestException as e:
            # Handle any request-related errors gracefully
            df_conversations.at[index, 'conversation'] = ""  # Mark as empty if an error occurs

    # Filter out rows with no usable conversation records
    df_conversations['conversation'] = df_conversations['conversation'].astype(str)
    print(f"Length of dataset with usable records: {len(df_conversations)}")

    # Instantiate the Privacy Filter
    filter_instance = LotteryPrivacyFilter()

    # Apply the privacy filter to the 'conversation' column and store in 'result'
    print("Applying PII filter on conversation text data...")
    df_conversations['result'] = df_conversations['conversation'].apply(filter_instance.filter)

    # Debug: Print transformation details
    for idx, row in df_conversations.iterrows():
        print(f"Row {idx + 1} - Original:", row['conversation'])
        print(f"Row {idx + 1} - Filtered:", row['result'])

    # Calculate elapsed time
    elapsed_time = time.time() - start_time
    print(f"Total conversations fetching time: {elapsed_time:.2f} seconds")

    return df_conversations

